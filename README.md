# ilos, a screen brightness manager using the webcam

In Greek mythology, Ilos was the founder of Troy, and brother of Erichthonius.

The king of Phrygia gave Ilos a spotted cow and advised him to found a city where the cow first lies down.

This is not in any way analagous to this program.

Ilos is a screen-brightness manager, and is named after an acronym:

+ In
+ Lieu
+ Of a
+ Sensor

Many laptop computers come with a brightness sensor to determine a healthy screen brightness for the user.
Ilos achieves the same goal in lieu of a sensor by utilizing an installed camera to estimate the brightness of one's surroundings.

## Dependencies

ilos depends on:

	light

(https://github.com/haikarainen/light)

	numpy

(https://numpy.org)

	OpenCV

(https://opencv.org)

## Running

You can invoke the module within the project directory with

	python3 -m ilos
